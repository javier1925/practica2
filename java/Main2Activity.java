package com.example.evotec.primeraapp4b;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        buttonAutenticar = (Button)findViewById(R.id.buttonAutenticar);
        buttonContraseña = (Button)findViewById(R.id.buttonContraseña);
        buttonUsuario = (Button)findViewById(R.id.buttonUsuario);

        buttonAutenticar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent4 = new Intent( Main2Activity.this, ActivityPrincipal.class);
                startActivity(intent4);
            }
        });
}
    Button buttonUsuario, buttonContraseña, buttonAutenticar;
}
