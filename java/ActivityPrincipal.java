package com.example.evotec.primeraapp4b;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class ActivityPrincipal extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
    }
}
