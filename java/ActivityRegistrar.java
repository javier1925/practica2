package com.example.evotec.primeraapp4b;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

public class ActivityRegistrar extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar);
        buttonR_Contraseña = (Button)findViewById(R.id.buttonR_Contraseña);
        buttonR_Crear = (Button)findViewById(R.id.buttonR_Crear);
        buttonR_Usuario = (Button)findViewById(R.id.buttonR_Usuario);
    }
    Button buttonR_Usuario, buttonR_Contraseña,buttonR_Crear;
}
