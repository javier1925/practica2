package com.example.evotec.primeraapp4b;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ActivityRecibirParametro extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibir_parametro);
        lblParametro = (TextView)findViewById(R.id.lblParametro);
        Bundle bundle = this.getIntent().getExtras();
        lblParametro.setText(bundle.getString("dato"));

    }

    TextView lblParametro;
}
