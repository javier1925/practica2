package com.example.evotec.primeraapp4b;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    buttonLogin = (Button)findViewById(R.id.buttonLogin);
    buttonRegistrarse = (Button)findViewById(R.id.buttonRegistrarse);
    buttonBuscarM = (Button)findViewById(R.id.buttonBuscarM);
    buttonPasarP = (Button)findViewById(R.id.buttonPasarP);

    buttonLogin.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(MainActivity.this, Main2Activity.class);
            startActivity(intent);
        }
    });
    buttonRegistrarse.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent2 = new Intent(MainActivity.this, ActivityRegistrar.class);
            startActivity(intent2);
        }
    });

    buttonBuscarM.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent3 = new Intent(MainActivity.this, ActivityBuscar.class);
            startActivity(intent3);
        }
    });

    buttonPasarP.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent4 = new Intent(MainActivity.this, ActivityPasarParametro.class);
            startActivity(intent4);
        }
    });

    }
    Button buttonLogin, buttonBuscarM, buttonRegistrarse, buttonPasarP;

}
