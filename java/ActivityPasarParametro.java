package com.example.evotec.primeraapp4b;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ActivityPasarParametro extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pasar_parametro);
        btnEnviarParametro = (Button)findViewById(R.id.btnEnviarParametro);
        txtPasarParametro = (EditText)findViewById(R.id.txtPasarParametro);

        btnEnviarParametro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new  Intent (ActivityPasarParametro.this, ActivityRecibirParametro.class);
            Bundle bundle = new Bundle();
            bundle.putString("dato",txtPasarParametro.getText().toString());
            intent.putExtras(bundle);
            startActivity(intent);

            }
        });

    }
    EditText txtPasarParametro;
    Button btnEnviarParametro;
}
