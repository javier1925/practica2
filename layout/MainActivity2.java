<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    tools:context=".MainActivity">

    <TextView
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="@string/Primer"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintLeft_toLeftOf="parent"
        app:layout_constraintRight_toRightOf="parent"
        app:layout_constraintTop_toTopOf="parent" />

    <Button
        android:id="@+id/buttonUsuario"
        android:layout_width="match_parent"
        android:layout_height="70dp"
        android:backgroundTint="@android:color/white"
        android:text="INGRESE USUARIO" />

    <TextView
        android:id="@+id/textView4"
        android:layout_width="match_parent"
        android:layout_height="51dp" />

    <Button
        android:id="@+id/buttonContraseña"
        android:layout_width="match_parent"
        android:layout_height="70dp"
        android:backgroundTint="@android:color/white"
        android:text="INGRESE CONTRASEÑA" />

    <EditText
        android:id="@+id/editText2"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:ems="10"
        android:inputType="numberPassword" />

    <Button
        android:id="@+id/buttonAutenticar"
        android:layout_width="match_parent"
        android:layout_height="70dp"
        android:backgroundTint="@android:color/white"
        android:text="AUTENTICAR" />

</LinearLayout>