<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".ActivityPasarParametro">

    <EditText
        android:id="@+id/txtPasarParametro"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_alignParentTop="true"
        android:layout_marginTop="47dp"
        android:layout_centerHorizontal="true"
        android:ems="10"
        android:inputType="textPersonName"
        android:hint="PORFAVOR INGRESE UN DATO"
        />
    <Button
        android:id="@+id/btnEnviarParametro"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_alignStart="@+id/txtPasarParametro"
        android:layout_below="@+id/txtPasarParametro"
        android:layout_marginStart="52dp"
        android:layout_marginTop="49dp"
        android:text="ENVIAR PARAMETRO"
        />
</RelativeLayout>